def train_model_similar_sentences(answer="hi"):
    """

    """
    from SimilarSentences import SimilarSentences
    model = SimilarSentences("sentences.txt", "train")
    model.train()

def get_similar_paragraph(question="how are you?"):
    """

    """
    from SimilarSentences import SimilarSentences
    import json

    model = SimilarSentences("model.zip", "predict")
    # simple = model.predict(answer, 2, "simple")
    detailed = model.predict(question, 2, "detailed")
    return json.loads(detailed)[0][0]


def get_answer_from_bert(context="", question=""):
    '''
    Takes a `question` string and an `answer_text` string (which contains the
    answer), and identifies the words within the `answer_text` that are the
    answer. Prints them out.
    '''
    from transformers import BertForQuestionAnswering

    model = BertForQuestionAnswering.from_pretrained(
        'bert-large-uncased-whole-word-masking-finetuned-squad')

    from transformers import BertTokenizer

    tokenizer = BertTokenizer.from_pretrained(
        'bert-large-uncased-whole-word-masking-finetuned-squad')

    import torch

    # ======== Tokenize ========
    # Apply the tokenizer to the input text, treating them as a text-pair.
    input_ids = tokenizer.encode(question, context)

    # Report how long the input sequence is.
    print('Query has {:,} tokens.\n'.format(len(input_ids)))

    # ======== Set Segment IDs ========
    # Search the input_ids for the first instance of the `[SEP]` token.
    sep_index = input_ids.index(tokenizer.sep_token_id)

    # The number of segment A tokens includes the [SEP] token istelf.
    num_seg_a = sep_index + 1

    # The remainder are segment B.
    num_seg_b = len(input_ids) - num_seg_a

    # Construct the list of 0s and 1s.
    segment_ids = [0] * num_seg_a + [1] * num_seg_b

    # There should be a segment_id for every input token.
    assert len(segment_ids) == len(input_ids)

    # ======== Evaluate ========
    # Run our example question through the model.
    start_scores, end_scores = model(torch.tensor([input_ids]),
                                     # The tokens representing our input text.
                                     token_type_ids=torch.tensor([segment_ids]))  # The segment IDs to differentiate question from answer_text

    # ======== Reconstruct Answer ========
    # Find the tokens with the highest `start` and `end` scores.
    answer_start = torch.argmax(start_scores)
    answer_end = torch.argmax(end_scores)

    # Get the string versions of the input tokens.
    tokens = tokenizer.convert_ids_to_tokens(input_ids)

    # Start with the first token.
    answer = tokens[answer_start]

    # Select the remaining answer tokens and join them with whitespace.
    for i in range(answer_start + 1, answer_end + 1):

        # If it's a subword token, then recombine it with the previous token.
        if tokens[i][0:2] == '##':
            answer += tokens[i][2:]

        # Otherwise, add a space then the token.
        else:
            answer += ' ' + tokens[i]

    return answer

def get_translate(text="", source="", target=""):
    """

    """
    text = "He is fine?"
    return text


def get_answer(pergunta="Ele esta bem?"):

    # Traduzir a pergunta
    question = get_translate(pergunta, source="pt-br", target="en")
    print("Pergunta traduzida para:")
    print(question)

    # Localizar paragrafo dentro de sentences.txt
    context = get_similar_paragraph(question)
    print("\nParagrafo similar: \n")
    print(context)

    # Invocar o BERT para achar a melhor answer
    answer = get_answer_from_bert(context,question)
    print("\nResposta do Bert: \n")
    print(question)

    # traduzir a answer
    resposta = get_translate(answer, source="en", target="pt-br")
    print("Resposta traduzida para: \n")
    print(resposta)

get_answer()